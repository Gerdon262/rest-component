const TerserWebpackPlugin = require('terser-webpack-plugin')

module.exports = {
  outputDir: 'public',
  configureWebpack: {
    plugins: [
      new TerserWebpackPlugin()
    ]
  }
}
